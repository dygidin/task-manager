import { DirectiveOptions } from 'vue'

const directive: DirectiveOptions = {
  inserted(el, binding, vnode) {
    let startX: number, startY: number, initialMouseX: number, initialMouseY: number;
    
    function mousemove(e: MouseEvent) {
      e.preventDefault();
      const dx = e.clientX - initialMouseX;
      const dy = e.clientY - initialMouseY;
      binding.value({
        id: el.getAttribute('id'),
        left: startX + dx,
        top: startY + dy,
        right: startX + dx + el.clientWidth
      })
      return false;
    }

    function mouseup(e: MouseEvent) {
      e.preventDefault();
      document.removeEventListener('mousemove', mousemove);
      document.removeEventListener('mouseup', mouseup);
    }

    el.addEventListener('mousedown', function (e) {
      const { left, top } = el.getBoundingClientRect();
      startX = left;
      startY = top;
      initialMouseX = e.clientX;
      initialMouseY = e.clientY;
      document.addEventListener('mousemove', mousemove);
      document.addEventListener('mouseup', mouseup);
      return false;
    });
  }
};

export default directive;