export type TCell = {
    id: string;
    left: number;
    top: number;
    width: number;
    height: number;
}