import moment from 'moment';
import { TimeLine, InterfaceTimeLine } from './timeline';

export class Utils {
  static getUniqId(): string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  static formatDate(date: Date, format: string) {
    return moment(date).format(format);
  }

  static calcPart(total: number, timeLine: InterfaceTimeLine): number {
    const from = parseInt(moment(timeLine.list[0].date).format('HH'));
    const to = parseInt(moment(timeLine.list[timeLine.list.length - 1].date).format('HH'));
    const countHours = (to - from + 1);    
    return (total / ((countHours * 60 * 60) / 60));
  }

  static calcPosToTime(pos: number, total: number, timeLine: InterfaceTimeLine): string {
    const diffHours = parseInt(moment(timeLine.list[0].date).format('HH')) - 1;
    const sec = (pos / (this.calcPart(total, timeLine) / 60)) + (diffHours * 60 * 60);
    return moment(new Date()).startOf('day')
      .seconds(sec)
      .format('HH:mm:ss');
  }

  static calcTimeToPos(time: Date, total: number, timeLine: InterfaceTimeLine): number {
    const diffHours = parseInt(moment(timeLine.list[0].date).format('HH')) - 1;   
    const sec = moment.duration(this.formatDate(time, 'HH:mm:ss')).asSeconds() - (diffHours * 60 * 60);
    return (sec / 60) * this.calcPart(total, timeLine);
  }

  static getDiff(date1: Date, date2: Date): number {
    const duration = moment.duration(moment(date2).diff(date1));
    return duration.asSeconds();
  }

  static addTime(date: Date, seconds: number): string {
    return moment(date).add('seconds', seconds).format('YYYY-MM-DD HH:mm:ss');
  }

  static isBetween(startDate: Date, endDate: Date, date: Date): boolean {
    const compareDate = moment(date);
    return compareDate.isBetween(startDate, endDate);
  }

}