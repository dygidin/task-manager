import { Column } from './column';

export type TSticker = {
  id: string;
  left: number;
  top: number;
  height: number;
  width: number;
  zIndex?: number;  
  from: Date;
  to: Date;
  name: string;    
  background?: string;
  icon?: string;
  column: Column;
}

export type TCoord = {
  id: string;
  left: number;
  top: number;
  right: number;
}