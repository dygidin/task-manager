import { Utils } from './utils';

interface InterfaceDateType {
  date: Date;
  getTimeStr(format?: string): string;
}

export class DateType implements InterfaceDateType {
  date!: Date;
  constructor(date: Date) {
    this.date = date
  }

  getTimeStr(format: string = 'HH:mm A'): string {
    return Utils.formatDate(this.date, format);
  }
}
