import { TSticker } from './sticker';
import { Utils } from './utils';

export class RecalcPosition {

  constructor(private stickers: Array<TSticker> = []) { }

  private getChildren(arr: Array<string>): Array<string> {
    const childrenItems: any = [];

    const getRecursive = (arr: Array<any>) => {
      for (let index = 0; index < arr.length; index++) {
        const element = arr[index];
        childrenItems.push(element.id);
        if (element.children.length) {
          getRecursive(element.children);
        }
      }
    };
    getRecursive(arr);
    return childrenItems;
  }

  private listToTree(arr: Array<{ id: string; parentId: string }>) {
    const tree = [],
      mappedArr: any = {};
    let arrElem: any, mappedElem;

    for (let index = 0; index < arr.length; index++) {
      const arrElem = arr[index];

      if (!arr.some(el => el.id === arrElem.parentId))
        arr.unshift({ id: arrElem.parentId, parentId: "" });
    }

    for (let i = 0; i < arr.length; i++) {
      arrElem = arr[i];
      mappedArr[arrElem.id] = arrElem;
      mappedArr[arrElem.id]["children"] = [];
    }

    for (const id in mappedArr) {
      if (mappedArr[id]) {
        mappedElem = mappedArr[id];

        if (mappedElem.parentId) {
          mappedArr[mappedElem["parentId"]]["children"].push(mappedElem);
        } else {
          tree.push(mappedElem);
        }
      }
    }
    return tree;
  }

  recalc(): Array<TSticker> {
    const list: Array<{ id: string; parentId: string; from: number }> = [];
    for (let i = 0; i < this.stickers.length; i++) {
      const root = this.stickers[i];

      for (let j = 0; j < this.stickers.length; j++) {
        const child = this.stickers[j];
        if (
          Utils.isBetween(root.from, root.to, child.from) &&
          root.column.id === child.column.id
        ) {

          list.push({
            id: child.id,
            parentId: root.id,
            from: child.from.getTime()
          });
        }
      }
    }

    list.sort((a, b) => a.from >= b.from ? 1 : -1);

    const tree: Array<{ id: string; children: Array<string> }> = this.listToTree(
      list
    );

    const groups: Array<any> = [];
    for (let index = 0; index < tree.length; index++) {
      const element = tree[index];
      groups.push(
        Array.prototype.concat(element.id, this.getChildren(element.children))
      );
    }

    const getIntersecting = (group: Array<string>, sticker: TSticker) => {
      let result = 0;
      for (let index = 0; index < group.length; index++) {
        const g = group[index];
        const s = this.stickers.find(sticker => sticker.id === g);
        if (s && (s.id !== sticker.id && sticker.top >= s.top && sticker.top <= s.top + s.height)) {
          result = getIntersecting(group, s) + 1;
        }
      }
      return result;
    }

    groups.forEach(group => {
      let i: number = 0;
      group.forEach((g: string) => {
        const sticker = this.stickers.find(sticker => sticker.id === g);
        if (sticker) {
          const count = getIntersecting(group, sticker);          
          sticker.zIndex = i + 1;
          sticker.width -= count * 20;
          sticker.left += count * 20;
          i++;
        }
      });
    });

    return this.stickers;
  }
}