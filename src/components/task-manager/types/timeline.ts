import { DateType } from './datetype';
import { Utils } from './utils';

export interface InterfaceTimeLine {
  list: Array<DateType>;
  addTime(from: number, to: number): void;
  calcPosToTime(pos: number, length: number): string;
  calcTimeToPos(time: Date, total: number): number;
}

export class TimeLine implements InterfaceTimeLine {
  readonly list: Array<DateType> = [];
  public el: HTMLElement;

  constructor(from: number, to: number) {
    this.addTime(from, to);
  }

  addTime(from: number, to: number) {
    for (let i = from; i <= to; i++) {
      let time: string = i + ':00';
      if (i < 10) {
        time = '0' + i + ':00';
      }

      const currentDate = new Date();
      const month: string = (currentDate.getMonth() + 1) >= 10 ? (currentDate.getMonth() + 1).toString() : '0' + (currentDate.getMonth() + 1).toString();
      const date: string = currentDate.getDate() >= 10 ? currentDate.getDate().toString() : '0' + currentDate.getDate().toString();
      
      this.list.push(new DateType(new Date(currentDate.getFullYear() + '-' + month + '-' + date + 'T' + time)));
    }
  }

  calcPosToTime(pos: number): string {
    if (!this.el) {
      throw ('TimeLine HTML element not found.');
    }
    const { height } = this.el.getBoundingClientRect();
    return Utils.calcPosToTime(pos, height, this);
  }

  calcTimeToPos(time: Date): number {
    if (!this.el) {
      throw ('TimeLine HTML element not found.');
    }
    const { height } = this.el.getBoundingClientRect();
    return Utils.calcTimeToPos(time, height, this);

  }
}