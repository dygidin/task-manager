import { Utils } from '../types/utils';
import { TSticker } from './sticker';

export interface InterfaceColumn {
  id: string;
  name: string;
  ava: string;
  stickers: Array<TSticker>;
  addSticker(sticker: TSticker): void;
  removeSticker(sticker: TSticker): void;
}

export class Column implements InterfaceColumn {
  id: string = Utils.getUniqId();
  stickers: Array<TSticker> = [];
  constructor(public name: string, public ava: string = '') {
  }

  addSticker(sticker: TSticker) {    
    this.stickers.push(sticker);
  }

  removeSticker(sticker: TSticker) {
    this.stickers = this.stickers.filter(i => i.id !== sticker.id);
  }
}